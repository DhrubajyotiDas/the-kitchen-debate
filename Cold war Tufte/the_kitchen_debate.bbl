\begin{thebibliography}{19}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[NYT({\natexlab{a}})]{NYT_4171959}
{Nixon will Visit Moscow in July to Open Exhibit}.
\newblock \emph{New York Times}, April 17, 1959, {\natexlab{a}}.

\bibitem[NYT({\natexlab{b}})]{NYT_5151959}
{A Typical Trip to Moscow Won by `Typical' Suburban Family}.
\newblock \emph{New York Times}, May 15, 1959, {\natexlab{b}}.

\bibitem[NYT({\natexlab{c}})]{NYT_7251959}
{Nixon and Khrushchev Argue In Public as U.S. Exhibit Opens; Accuse Each Other
  of Threats.}
\newblock \emph{New York Times}, July 25, 1959, {\natexlab{c}}.

\bibitem[NYT({\natexlab{d}})]{NYT_7251959_2}
{The Two Worlds: A Day-Long Debate}.
\newblock \emph{New York Times}, July 25, 1959, {\natexlab{d}}.

\bibitem[NYT({\natexlab{e}})]{NYT_7281959_2}
{Soviet TV Shows Tape of Debate}.
\newblock \emph{New York Times}, July 28, 1959, {\natexlab{e}}.

\bibitem[Carbone(2009)]{Carbone}
Cristina Carbone.
\newblock {\emph{Staging the Kitchen Debate: How Splitnik Got Normalized in the
  United States}}.
\newblock In Ruth Oldenziel and Karin Zachmann, editors, \emph{Cold War
  Kitchen}. The MIT Press, London and Massachusetts, 2009.

\bibitem[Castillo(2005)]{Castillo2005}
Greg Castillo.
\newblock {Domesticating the Cold War: Household Consumption as Propaganda in
  Marshall Plan Germany.}
\newblock \emph{Journal of Contemporary History}, 40\penalty0 (2):\penalty0
  261, 2005.

\bibitem[Castillo(2009)]{Castillo}
Greg Castillo.
\newblock {\emph{The Americans' `Fat Kitchen' in Europe: Postwar Domestic
  Modernity and Marshall Plan Strategies of Enchantment}}.
\newblock In Ruth Oldenziel and Karin Zachmann, editors, \emph{Cold War
  Kitchen}. The MIT Press, London and Massachusetts, 2009.

\bibitem[{Cynthia Kellog}()]{Kellog}
{Cynthia Kellog}.
\newblock {American Home in Moscow}.
\newblock \emph{New York Times Magazine}, July 5, 1959.

\bibitem[{Felix Belair Jr.}()]{NYT_1241959}
{Felix Belair Jr.}
\newblock {President Backs U.S. Fair in Soviet}.
\newblock \emph{New York Times}, January 24, 1959.

\bibitem[Gaddis(2006)]{Gaddis}
John~Lewis Gaddis.
\newblock \emph{The Cold War: A New History}.
\newblock Penguin Books, New York, 2006.

\bibitem[Hixson(1998)]{Hixson}
Walter~L. Hixson.
\newblock \emph{Parting the Curtain: Propaganda, Culture, and the Cold War,
  1945-1961}.
\newblock Palgrave Macmillan, New York, 1998.

\bibitem[{Jack Gould}()]{Gould}
{Jack Gould}.
\newblock {TV: Debate in Moscow}.
\newblock \emph{New York Times}, July 27, 1959.

\bibitem[{James Reston}()]{Reston}
{James Reston}.
\newblock {A Debate of Politicians}.
\newblock \emph{New York Times}, July 25, 1959.

\bibitem[Oldenziel and Zachmann(2009)]{Oldenziel}
Ruth Oldenziel and Karin Zachmann.
\newblock {\emph{Kitchen as Technology and Politics: An Introduction}}.
\newblock In Ruth Oldenziel and Karin Zachmann, editors, \emph{Cold War
  Kitchen}. The MIT Press, London and Massachusetts, 2009.

\bibitem[Potter(1954)]{Potter}
David Potter.
\newblock \emph{People of Plenty: Economic Abundance and the American
  Character}.
\newblock University Of Chicago Press, Chicago, 1954.

\bibitem[{Richard Shepard}()]{Shepard}
{Richard Shepard}.
\newblock {Debate Goes on TV over Soviet Protest}.
\newblock \emph{New York Times}, July 26, 1959.

\bibitem[Stitziel(2005)]{Stitziel}
Judd Stitziel.
\newblock \emph{Fashioning Socialism: Clothing, Politics and Consumer Culture
  in East Germany}.
\newblock Bloomsbury Academic, New York, 2005.

\bibitem[Taylor(2005)]{Taylor}
Brian~D. Taylor.
\newblock \emph{{The Dancer Defects: The Struggle for Cultural Supremacy during
  the Cold War}}.
\newblock Oxford University Press, Oxford, 2005.

\end{thebibliography}
