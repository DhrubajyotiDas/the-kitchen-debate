\input{header}
\begin{document}

\frontmatter
\maketitle
\thispagestyle{empty}
\newpage
\begin{fullwidth}
~\vfill
\thispagestyle{empty}
\setlength{\parindent}{0pt}
\setlength{\parskip}{\baselineskip}
Copyright \copyright\ \the\year\ \thanklessauthor
\par\smallcaps{Published by The Cooper Union}
\par Typeset using \LaTeX with the \texttt{tufte-book} class and Minion Pro typeface
\par\emph{First printing, April 2014}
\end{fullwidth}
\newpage
\morefloats

\mainmatter
\newthought{Post World War ii}, the Marshall Plan, officially known as the European Recovery Program, had taken on the considerable task of both building up the shattered economies of Europe and trying to prevent them from sliding towards Communism. 



One of its most prominent tools was to publicize \emph{commodity fantasy} including promoting the American \emph{fat kitchen} to jump-start low-cost mass consumption of consumer goods.\cite[0\baselineskip]{Castillo}
The humble kitchen of the nineteenth century had progressed into a
\begin{quote}
%\begin{fullwidth}
	unified, modular ensemble \ldots connected to \ldots electrical grids, gas networks, water systems, and the food chain \ldots [coming] together via an intricate web of large technical systems

%\end{fullwidth}
\end{quote}
that signified the technological marvel known as the modern kitchen.\cite{Oldenziel}
The Kitchen had become an object of immense symbolic importance, and a vivid example of technopolitics, used by politicians to ``constitute, embody, and enact their political goals.''
Besides Nixon and Khrushchev at the American National Exhibition in Moscow, other Cold War politicians, including Winston Churchill, Ludwig Erhard and Walter Ulbricht considered the kitchen to be one of the building blocks for the ``social contract between citizens and states.''
While the U.S. and the Soviet Union differed in their definition of this contract, they did agree upon the importance of science and technology in determining a society's progress, transforming the kitchen where this science and technology played such a major and intimate role, into a very important Cold War arena. 

The historian David M. Potter wrote \cite[0\baselineskip]{Potter} in his 1954 study:
\begin{quote}
  %\begin{fullwidth}
    As a result, our message to the world has become involved in a dilemma: to other peoples, our democracy has seemed attainable but not especially desirable; our abundance has seemed infinitely desirable but not quite attainable. But, if the realities of the relationship between democracy and abundance  had been understood by people of other countries or, what is more to the point, by those Americans who were seeking to impart our message, our democracy would seem more desirable, and our abundance would have seemed more attainable. Both these changes would have had the effect of strengthening the moral influence of the United States.
  %\end{fullwidth}
\end{quote}
This effort to clarify the relationship between democracy and abundance required a significant campaign to familiarize Europeans with American lifestyles and its consumerist material culture. 
By 1947, the U.S. was aware of Soviet propaganda ridiculing ``the American way of life'', setting off frantic counter-propaganda measures aimed at pitching the ``try it our way.''   
These included spreading awareness about the standard of living and amenities enjoyed by average Americans.


Initial attempts were stillborn, such as the modestly attended \emph{So wohnt Amerika},\marginnote{How America Lives} an exhibition of photos and scale models of suburban homes in Frankfurt in 1949.
U.S. propaganda specialists, however, soon realized the attraction that consumer appliances held for European audiences, and made a point of including them, even at the considerable effort of bringing such gadgets together, at all future exhibitions.\sidenote[][-3\baselineskip]{``If real honest-to-god electric stoves, refrigerators and deep-freeze units had been on hand, the general attendance figures would have been astronomic'', as the head of Frankfurt's U.S. Information Center, Donald W. Muntz put it. From \citep[p. 39]{Castillo}} 
Such exhibitions were in no short supply; the idea of displaying an actual fully functional suburban home with all appliances installed was but a logical extension, and was tried out in the West Berlin Industrial Fair of 1950 for the first time. 
Unsurprisingly, when \emph{Amerika zu Hause} \sidenote{America at Home} opened, it was mobbed by 43,000 visitors in two weeks, including 15,000 from East Berlin, for whom, quite astutely, the admission price was lowered. 
A house within a house, an ``ideal dwelling'' was first seen at the 1952 \emph{Wir bauen ein besseres Leben},\sidenote{We're Building a Better Life} complete with ``man-wife-child family team actually going through the physical actions of living in the dwelling, making proper use of the objects in it.''
Suggestively, the show's theme was instructed by the State Department to place emphasis upon ``the fortunate outcome of American economic philosophy when combined with European skills and resources.'' \cite[0\baselineskip]{Castillo} 
On cue, a billboard right by the house proclaimed that the 
\begin{quote}
%\begin{fullwidth}
	objects in this house are industrial products from many countries in the Atlantic community. Thanks to technology, rising productivity, economic cooperation, and free enterprise, these objects are available to our western civilization.

%\end{fullwidth}
\end{quote}
Considering the enormous popularity and interest that these exhibitions stoked in both West and East Germans, this effort was a home run for the Americans.


The Eastern bloc also began to catch on to the American tactics and started promoting its own form of consumerism, a sort of ``citizen enfranchisement through consumer rewards.'' 
A new form of socialist advertising employed distinctly western means -- the Czechoslovakian city of Pilsen hosted a multi-room home interior pantomime in 1957, mocked up along a department store entrance, featuring professional actors portraying a family engaged in domestic activities, with the model housewife demonstrating an array of new domestic products and appliances to spectators on the sidewalk. 
This  approach garnered great interest among the public in Pilsen, mirroring the response in Berlin five years earlier.

\newthought{The American National Exhibit} came close on the heels of the 21st Congress of the Communist Party of the Soviet Union, which Khrushchev, after having squashed the Anti-Party group's attempted coup against him in 1957, had used to consolidate political strength.\cite[0\baselineskip]{Taylor}
\marginnote{The Congress confirmed Nikita Khrushchev to be the undisputed ruler of the country, the successor of Lenin and Stalin, and the authoritative interpreter of Marx
He was credited with every Soviet achievement and any organized opposition was totally humiliated or destroyed
It also provided Khrushchev with the license to continue the liberal reforms he had initiated after Stalin's death.
From American Jewish Committee Archives, (1960), 255.} 
This was the U.S.'s reciprocal gesture to the Soviet exhibit in New York City in 1958 and was a continuation of an agreement signed on December 29 of the same year between the two states agreeing to increase cultural contact.
The agreement had specifically stated the ``usefulness of exhibits as an effective means of developing mutual understanding.''
Vice President Richard Nixon represented the United States at the exhibition at the request of President Eisenhower.
This was the first time such a high ranking government official visited the Soviet Union since Roosevelt's trip to the Yalta Conference.
The White House said that \cite{NYT_4171959} ``Mr.~Nixon's trip could hardly be called an \emph{official} visit and that there were no plans of a meeting with Khrushchev'', although later it was revealed that it was the Deputy Premier of the Soviet Union, Anastas Mikoyan, who had informally suggested that Nixon visit Moscow when he visited the U.S. the previous winter.

The official aim of the U.S. in opening the American National Exhibit, as stated by President Eisenhower at a press conference, was to ``achieve a broader understanding between our two peoples -- the kind of understanding upon which our peaceful future depends.'' 
Foremost among his hopes for the exhibition was that it would ``show the Russians a \emph{true image} of the American people, of the progress here in science, technology and culture, and of the United States' desire for peace.'' \cite{NYT_1241959} 
The New York Times reported that the Exhibit was planned to ``show [how] the United States lives, works, produces, consumes and plays.''    
Coupled with the publicly stated aim, a secret objective of the exhibition was also published\cite[0\baselineskip]{Carbone} in a paper entitled ``Secret Basic Policy Guidance for the U.S. Exhibit in Moscow in 1959,'' that espoused a parallel but separate goal of placing 
\begin{quote}
%\begin{fullwidth}
  particular emphasis on American precepts, practices and concepts which might contribute to existing pressures tending in the long run toward a reorientation of the Soviet system in the direction of greater freedom.

%\end{fullwidth}
\end{quote}
``The displays should endeavor to make the Soviet people dissatisfied with the share of the Russian pie that they now receive,'' was the American ambassador to the Soviet Union, Llewellyn Thompson's, explicit advice on exhibition strategies.\cite{Castillo2005} 
In stark contrast to the Soviet Exhibit in New York, where the Soviet Union had focused on national achievements and made the Sputnik, the first successful man-made satellite to orbit the earth, their prime display,  the US chose to focus on the achievements of common Americans and what the country had achieved in raising their standard of living.

\begin{figure*}[h]
  \begin{center}
    \includegraphics[width=0.75\columnwidth]{Images/Picture1.pdf}
  \end{center}
  \caption{\small{Geodesic dome at the American National Exhibition, Moscow, Russia. By Carl Mydans for Life, September 1959. \copyright Time Inc.}}
\end{figure*}

The total budgeted cost of the American exhibition was around \$5 million with the US government pledging \$3.6 million and the rest contributed by private companies like Pepsi Co., IBM, Macy's, and others that were going to set up stands at the exhibition.
Eisenhower claimed it to be ``about the best investment of money this Government has made in a long time.''
\cite[\baselineskip]{NYT_4171959}
An audience of about 5,000 visitors per hour was expected to converge upon Sokolniki Park on the outskirts of Moscow, accessible by a fifteen minute subway ride.
The principal building included a geodesic dome seventy-eight feet high resembling an umbrella resting on its rib ends.
Geodesic domes were a popular structure at trade fairs organized by the Office of International Trade Fairs during the Eisenhower administration, in keeping with the official modernistic architectural style adopted at the Moscow fair.
The dome received praise from many quarters when it was being constructed, including Premier Khrushchev himself, who called it ``one of many interesting technical innovations that [I] saw today.''
\sidenote[][-6\baselineskip]{
Khrushchev's appreciation was to be expected as the architectural style of the exhibition was according to his tastes. 
One facet of the de-Stalinization of the Soviet Union was the discouragement of the classically ornate Victorian style of architecture that Stalin actively encouraged and according to the British architect, Sir Hugh Casson, made Moscow in 1954 resemble Manchester a century ago.
Khrushchev intended to introduce a distinctly modern taste in new buildings, instructing his architects to adapt to styles that were compatible with mass production, leading to a spurt in mass-produced housing built mostly of prefabricated panels. 
The unmistakable modern design of the geodesic dome surely appealed to Khrushchev.
From \cite{Taylor}}

\begin{figure*}[t]
  \begin{center}
    \includegraphics[width=0.6\columnwidth]{Images/Picture2.pdf}
  \end{center}
  \caption{\small{Nixon and Khrushchev in front of the model kitchen, surrounded by the press. To Nixon's left is Leonid Brezhnev (eyes closed), who was previously trying to prevent the photographer William Safire from taking Khrushchev's picture. July 24, 1959 \copyright AP}}
\end{figure*}


\begin{figure*}[h]
  \begin{center}
    \includegraphics[width =0.75 \columnwidth]{Images/Picture3.pdf}
  \end{center}
  \caption{\small{\emph{"Please come along behind us."}
 Screenshot from color footage of the debate.  ``The Kitchen Debate (Nixon and Khrushchev, 1959)'' August 24, 2009, www.Youtube.com,
\url{http://www.youtube.com/watch?v=D7HqOrAakco} (accessed March 21, 2010). \copyright Ampex Data Systems.
}}
\end{figure*}

The total floor space was 30, 000 square feet and it was the primary information center about the United States. 
Inside, along the perimeter, there were exhibits on science and research, education, labor, productivity, health, social services and agriculture. 
An ``American Gallery'' on the outside displayed figures of Abraham Lincoln, Mark Twain, Benjamin Franklin and others. 
A separate 50,000 square feet exhibition hall was set up to show, as the New York Times called,\cite[0\baselineskip]{NYT_1241959} 
\begin{quote}
%\begin{fullwidth}
\ldots the results of the American system. The purpose here will be to show the abundance of the American economy as it is broadly shared by all the people, the immense variety and the great freedom of choice and conveniences available.  
  
%\end{fullwidth}
\end{quote}

This hall had all the exhibits which the Americans were very interested in showing to the Russians:  ready-mix and frozen foods, radios, television sets, IBM computers, phonographs, kitchen equipment, clothing, equipment for hobbies, and indoor and outdoor sports, automobiles and other travel possibilities, and diversions readily available in literature and the arts. 
A pre-fabricated house, replete with all modern electronic appliances, a model farm, and a show of jeeps, trucks, trailers and boats was set up outdoors, and a small admission fee was charged to facilitate counting the number of visitors.
The Americans were very careful in the design of the house.
The last thing they wanted to do was to ``over-design'' the house and present the Soviets with a ``false, Utopian picture'' of American housing.
In keeping with the spirit of authenticity, everything inside the house could have been purchased directly from Macy's department store, which in fact sent its in-house designer to Moscow to design the interior of the house for an imaginary middle-class American family of four.
The New York Times said \cite[0\baselineskip]{Kellog} that the furnishings in the house presented a 
\begin{quote}
  %\begin{fullwidth}
    realistic picture of the American home... within the means of a family with an annual income of \$5,200 and a furnished apartment for a family in the \$12,000 bracket.
  %\end{fullwidth}
\end{quote}
An actual suburban family was chosen by the Fashion Industries Committee for the American Exhibit in Moscow as the `Typical Suburban Family' to represent the U.S. at Sokolniki for six weeks.\cite[-3\baselineskip]{NYT_5151959}


\newthought{It was in a kitchen} in the model house in the second exhibition hall that Nixon and Khrushchev met and had the famous impromptu ``Kitchen Debate''.\cite[-3\baselineskip]{Carbone} 
\begin{quote}
  %\begin{fullwidth}
    In what is one of the most iconic images of the cold war, American Vice President Richard M. Nixon and Soviet Premier Nikita S. Khrushchev lean over the railing of the General Electric kitchen in the model home at America's national exhibition in Moscow in 1959. Standing in the passageway that ran down the center of the model home, Nixon looks as if he is lecturing to Khrushchev. Boxes of \textsc{s.o.s}. souring pads and Dash laundry detergent are visible in the corner, sitting atop a washing machine. It was in this pseudo-domestic setting that two men debated the quality of American and Soviet rockets and appliances, treating each with \emph{gravitas}.
  %\end{fullwidth}
\end{quote}
This exchange, unlike any other that ``has occurred within the memory of the gray-haired member of the Moscow or Washington press corps'', seemed more like ``an event dreamed up by a Hollywood script writer than a confrontation of two of the world's leading statesmen'', the \emph{New York Times} noted.\cite[0\baselineskip]{NYT_7251959} 
Starting in the Presidium Building in the Kremlin and reaching a climax in front of color television cameras at the exhibition, this lively argument about the ``merits of washing machines, capitalism, free exchange of ideas, summit meetings, rockets and ultimatums'', although sometimes sharp, remained informal and good-natured, and neither Nixon nor Khrushchev lost their temper.


The Kitchen Debate itself was a part of a longer exchange between the two leaders, although it received the most publicity as it was conducted impromptu in front of hundreds of journalists that hung on to every word uttered by the two.  
The first part happened in front of Ampex color television cameras (a major technological breakthrough in themselves, and a fact that Nixon did not forget to remind Khrushchev of), with Nixon's statements being translated to Khrushchev and vice versa as both men fired off point and counter-point in quick succession, standing at the podium to address the press together.
Then the men took a walk through the exhibition before reaching the eponymous model kitchen where a second salvo was fired, with Nixon pointing out to Khrushchev all the modern innovations present in the kitchen ready for direct installation into houses.
Focusing on the fact that any average American, even a steelworker making \$3 an hour could afford such a house with such luxuries, Nixon made the case for economic prosperity in the U.S.
Khrushchev replied by claiming that all Soviet citizens have such appliances in their houses, and that everyone is entitled to such amenities by virtue of just being born in the Soviet Union, unlike America, where without a dollar in one's pocket, one is nothing.
This back and forth exchange continued for a while, as neither men agreed to cede ground, as Khrushchev's ``capacity for catch-as-catch-can conversation and Nixon's ability to field rhetorical line drives''
\cite[0\baselineskip]{NYT_7251959} 
enthralled everyone present there.


The videotape \marginnote[\baselineskip]{I used the color footage of the press conference to quote Nixon verbatim, and relied on the \emph{New York Times}' edited transcript published as ``The Two Worlds: a Day-Long Debate'' on July 25, 1959, for the Russian parts of the exchange, and for the subsequent conversation in front of the model kitchen. 
I have cited both sources wherever relevant.} of the ``debate'', informative in its own respect, is nonetheless extremely entertaining, with Khrushchev alternately resembling a seasoned politician and an excited child, with Nixon trying his best to not appear frustrated at being cutoff mid-sentence repeatedly.
Although the exchange in front of the kitchen itself was not videotaped, the dialogue immediately preceding it was, and an audio recording of the kitchen argument exists.


The conversation between Nixon and Khrushchev had begun at the gate of the exhibition itself, with Khrushchev letting Nixon know what he thought of the United States' recent ban on the shipment of strategic goods to the Soviet Union.
\footnote[][-\baselineskip]{Resumption of trade with the U.S. was very high on Khrushchev's agenda, and it was something that had been repeated by him and by his deputy Mikoyan several times to Eisenhower and the State Department.
While in 1947 the U.S. exchanged close to \$700 million in exports and \$225 million in imports with Communist countries, that number had dropped to around \$113 million in exports and \$64 million in imports by 1957.
The State Department had bluntly put forward the following reasons: there wasn't much to be gained by this sort of trade, and more importantly, the trade in machinery and technical knowledge was only going to help the Communist countries build up industrially, economically and militarily.
Reported in the \emph{Kentucky New Era}, September 4, 1959.} 
The press-conference/debate started soon after the cameras began rolling with Nixon and Khrushchev walking into the frame continuing an ongoing conversation.
Nixon tried to make Khrushchev comfortable by explaining that the lights in the room had to be very bright for the color camera to pick up an image and that's why it was so hot in there. 
Pointing at the cameras, Nixon insisted that there must be free exchange of ideas between both countries, a call for transparency and dialogue that he repeated numerous times during the debate.

Nixon allowed Khrushchev to start the proceedings with an address,
\footnote[][1\baselineskip]{Surprisingly, or rather not so surprisingly, during the period 1957-61 Khrushchev, openly, repeatedly and ``blood-curdlingly'' threatened the West with nuclear annihilation, albeit with non-existent rockets.
From \cite{Gaddis}} his words being translated to Nixon in the background by an interpreter.
Khrushchev agreed enthusiastically with Nixon's idea, and true to his humble roots, added that if the U.S. and the Soviet Union remained in friendship, then other countries would too, and that
\begin{quote}
  %\begin{fullwidth}
    if there is a country that is too war-minded we could pull its ears a little and say: Don't you dare; fighting is not allowed now; this is a period of atomic armament; some foolish one could start a war and then even a wise one couldn't finish the war.
  %\end{fullwidth}
\end{quote}

Nixon would later politely characterize Khrushchev's remarks ``as expected -- sweeping and extemporaneous'' - it was the typical fist-waving and pompous fare that Khrushchev was famous for.
\footnote[][-\baselineskip]{Khrushchev liked to intimidate American diplomats and visitors; in a conversation with Hubert Humphrey he circled Humphrey's hometown, Minneapolis, in a big blue pencil to make sure he remembered ``to order them to spare the city when the rockets fly.'' From \citep{Gaddis}}
He started off by incorrectly guessing how long America had been independent, and juxtaposed Nixon's correct reply of about one hundred and fifty years with the Soviet Union's forty-two to show how much more rapidly the Soviet Union had progressed than the U.S. 
He then made his famous prediction:\cite[0\baselineskip]{NYT_7251959_2} 
\begin{quote}
  %\begin{fullwidth}
    \ldots and in another seven years we will be on the same level as America. When we catch you up, in passing you by, we will wave to you. Then if you wish we can stop and say: Please come along behind us.
  %\end{fullwidth}
\end{quote}



Khrushchev continued by expressing his displeasure at the recent declaration of the third week of July by the U.S. as Captive Nations Week,
\footnote[][0\baselineskip]{Captive Nations Week has been annually celebrated on the third week of July to raise public awareness of the oppression of nations under the control of Communist and non-democratic governments. 
As part of the United States Cold War strategy, Captive Nations Week was declared by a Congressional resolution in 1953 and signed into law by President Dwight D. Eisenhower in 1959.} 
something which he wanted to remedy in front of U.S. cameras by hugging a Soviet workman and asking if he looked like a slave laborer. 
Nixon chose not to indulge in Khrushchev's rhetoric, and reaffirmed his mantra of ensuring that ``Soviet and American [men], work together well for peace...This is the way it should be.''
Acting as a gracious host, he did not want to be blamed for starting a shouting match with Khrushchev, and for the most part his tone was conciliatory and his words uncontroversial. 
However, not letting Khrushchev bully him, Nixon fought back by pointing out the color video cameras that were recording their every word as 
\begin{quote}
  %\begin{fullwidth}
    \ldots one of the best means of communication ever developed, and I can only say that this competition which you have just described in which you plan to outstrip us and particularly in the production of consumer goods, and if this competition is to do the best for both of our peoples and for people everywhere, there must be a free exchange of ideas. After all you don't know everything.
  %\end{fullwidth}
\end{quote}
Nixon relented on the fact that the Soviet Union were ahead of the U.S. in the thrust of their rockets before Khrushchev could remind him of the fact and score some points, but in the same breath he added that the U.S. was ahead of the Soviet Union in color television, leaving Khrushchev with nothing but a transparent, yet empty, boast that they had bettered the U.S. in this regard as well.

Pressing forward his advantage, Nixon reaffirmed to Khrushchev that his words would be translated and broadcast all over the U.S and at the same time obtained a guarantee from Khrushchev that his words would be translated and broadcast over the Soviet Union, repeatedly stressing the fact that ``You must not be afraid of ideas.''
The ebullient Khrushchev replied that it was they who were telling the Americans ``not to be afraid of ideas... the Soviet Union has already broken free from such a situation.''
One of the lighter moments of the debate come about when Khrushchev agrees with Nixon and then turns around and asks his aide ``What did I agree on?''


After shaking hands on agreeing to translate and broadcast each other's message at the exhibition in their respective countries, both men made make their way to the model house, where they had the second part of the debate.
Khrushchev had famously criticized this model house
\footnote[][-18\baselineskip]{A model house of some sort had been a permanent fixture at all American exhibitions, except the Brussels World's Fair of 1958, where instead of a self-contained structure, the various appliances and furniture were laid out on thematically grouped ``islands'' with the visitor assembling the furniture and appliances mentally to form a picture of the house.
The Brussels Fair had been almost universally condemned as a failure, and had spurred Eisenhower to take a personal interest in the Moscow Exhibition to undo the public relations damage.
Even then, the final decision to include a house in the Moscow exhibition was spurred on after a letter from a certain Mrs. W.A. Rice of Kentucky to her senator where she wrote that including a house would be a good way to ``let the Russians know how we live and give them ideas other than war making.'' She had then gone on to specify the dimensions of this house, the layout of the rooms, the design of the curtains, upholstery, cushions, box springs, and the color scheme.
The model home was the final element added to the exhibition after the senator forwarded the letter to the General Manager of the exhibition, who noted specifically that this item be given a ``very high priority on the list of exhibitions that we would like to display in Moscow.''}
two months ago when it was being built for being ``a capitalist lie that was perpetrated on the Soviet people.''\cite[0\baselineskip]{Carbone} 
It was obvious why Nixon had chosen a backdrop of a domestic kitchen to lecture Khrushchev about the merits of capitalism -- the domestic kitchen was one of the areas in which the U.S. held an unparalleled advantage over the Soviet Union, Khrushchev's boasts notwithstanding.
If Khrushchev was given the choice to stage the meeting, he would have similarly chosen a backdrop of space rockets and missiles.
Standing beside the General Electric canary yellow kitchen, Nixon pointed out all the labor saving devices available to make life easier for the American woman.
Not to be outdone, Khrushchev retorted that the Soviet Union did not possess such a ``capitalist attitude toward women.'' 
Nixon then focused on the affordability of such a house for the average American, using the example of the steelworker making \$3 an hour.
Khrushchev, again not wanting to be outdone, claimed that the Soviet Union had steel workers and peasants that could afford such a house: 
``besides, we built our houses firmly to last till their grandchildren'', 
unlike the Americans, whose houses lasted for only twenty years.
\footnote[][-9\baselineskip]{Khrushchev had already commented about the ``frailty'' of American houses when he visited the exhibition grounds two months ago.
Apparently he had heard ``from an American builder who had admitted to [him]'' that after twenty years, once the final installment was paid, the owner had to start building a new one as the houses don't last a long time.
Pravda had already by then ran an article about the house being beyond the reach of average Americans, and Khrushchev's words found an even more receptive audience.} 
Careful not to contradict Khrushchev, Nixon pointed out that either way, after twenty years many Americans wanted a new kitchen as the old one would have been obsolete by then, adding that ``the American system is designed to take advantage of new inventions and new techniques.''\cite[0\baselineskip]{NYT_7251959_2}


\newthought{The leaders matched wits} on the necessity of so many gadgets, with Khrushchev famously asking if the Americans had a gadget that ``puts food into the mouth and pushes it down?''  
The discussion then turned to more serious matters with Khrushchev trying to draw Nixon into a conversation about the ability of their engineers to crunch numbers for launching missiles, but Nixon deflected by talking about his dislike for the jazz music playing in the exhibition hall.
\footnote[][-\baselineskip]{Possibly a reference to the U.S.'s denials that they did not monitor or direct the content on  Voice of America and other such radio channels under the First Amendment -- channels that the Soviet Union actively prevented from broadcasting by jamming radio signals.
This however had little effect on the popularity of jazz itself -- more people tuned in to ``Music USA'' than any other radio program, ``making it probably the most effective propaganda coup in U.S.'s history.''  ``What are the Origins of American Jazz?'', ``What is the Present Direction in the Development of American Jazz?'', ``Most Popular U.S. Jazz Band?'' were the third, fourth, and fifth most frequently asked questions to the guides at the exhibition. 
Although the Soviet authorities had rejected the U.S.'s plan to have a live jazz band perform at the exhibition, they could not prevent them from playing recorded jazz music in the exhibition hall itself. 
From \emph{Parting the Curtain} by Walter Hixson.} 
He emphasized the importance that Americans gave to the power to choose -- 
``to us, diversity, the right to choose, the fact that we have 1,000 builders building 1,000 different houses is the most important thing.''\cite{NYT_7251959_2}
Nixon expanded upon this point later on as well, by making it clear that the Americans' aim was not to astonish the Russians but rather to show them the diversity present in the U.S.
He suggested that it would be better if they competed in this sector, 
``in the relative merits of washing machines than in the strength of rockets.''


The exhibition not only showed the interest that American consumerism spawned in the Soviet citizens, but also demonstrated the disconnect that existed between the government authorities priorities and what their people really wanted.
The exhibition was undoubtedly a tremendous success with the Soviet audience.
An estimated 140,000 visitors surged through the exhibition the opening weekend, and the heavy representation of Communist party members and agitators only led to an increased demand for tickets by the average citizen.\cite{Hixson}
The guides at the exhibits, ``articulate, attractive, and thoroughly briefed for the mission, had a definite positive impact on the Soviet visitors'' as they fielded numerous questions about American life and its people, concentrating on correcting some of the ``fantastic misconceptions'' that their audience held.
Even though the only books allowed at the  exhibition were the ones that had passed strict Soviet censorship scrutiny, visitors made off with hundreds of them from display shelves, including an astounding 600 ( and 14 Bibles) on the first day itself.
\footnote[][0\baselineskip]{An American diplomat reported, with some satisfaction, that ``the book corner is a favorite area, as evidenced by the number of items which have been disappearing.''} 
CPSU\sidenote{Communist Party of the Soviet Union} officials reported seeing people making off with used Pepsi-Cola cups while others pleaded for the same from the automatic dispensers.
The men were enraptured\footnote{One visitor had to be remonstrated by CPSU officials for ``almost begging the guide to sell the exhibited cars.'' From Hixson, Parting the Curtain.} 
by the auto exhibit, while the women, barred from receiving cosmetic samples, had to be content by observing the beauticians perform facial and hair treatments.
The Walt Disney Circarama and the ``Family of Man'' photographs were also centers of great interest.


\newthought{The Kitchen Debate received} immense publicity from the press, mainly because it was so completely unexpected and unscripted, and such a rare glance at a no-holds barred interaction between two of the most powerful men on the planet at the time.
William Safire, then a public relations specialist, later a speechwriter for Nixon and the most famous columnist for the New York Times, was present there and took the most iconic picture of Nixon and Khrushchev in front of the kitchen.
The color recording of the interview was broadcast on the three network televisions in the U.S. the following night.
Even though Nixon and Khrushchev had agreed that the tape be broadcast simultaneously in both countries, it was shown on Soviet television only two days later and that, too, at 11:45 pm at night, after ``a late dramatic play.'' \cite[0\baselineskip]{NYT_7281959_2}
ABC, CBS and NBC were initially asked to withhold broadcasting the tape by the State Department as per the request of the Russian Embassy, but the networks decided it was too important a news item to be delayed for too long over concerns that it might jeopardize Nixon's still on-going visit, and households across the U.S. saw the debate at 11 pm on Saturday, July 25.\cite{Shepard}
The New York Times called the debate ``unusual programming for home viewers'' and noted that Soviet authorities had protested the broadcast of the debate before they had a chance to examine the tape for simultaneous showing. 


The exhibition and the debate were covered extensively by newspapers.
Among a series of news reports about the exhibition, the New York Times also published a translated transcript of the exchange between the two leaders the following day.
A concise summary\cite[0\baselineskip]{NYT_7251959_2} of the proceedings included 
\begin{quote}
  %\begin{fullwidth}
    Mr. Nixon's curt and frank opposition to the points the Soviet leader kept jabbing home with his pudgy finger brought more jabs and a feeling of electric excitement to those informal public exchanges at Sokolniki Park.
  %\end{fullwidth}
\end{quote}
James Reston of the Times\cite{Reston} paralleled the conversation between Nixon and Khrushchev to 
\begin{quote}
  %\begin{fullwidth}
    very much like what would have happened if Nixon and Truman had made a trip through a country fair, arguing about the relative merits of the Republican and Democratic parties,
  %\end{fullwidth}
\end{quote}
while Jack Gould\cite{Gould} focused on the important role television played in personalizing such a historic moment.
Reston's analysis a few days later explained some of the most important conclusions drawn from Nixon's visit -- the most important, and the one that fulfilled the primary goal of the exhibition, being that 
\begin{quote}
  %\begin{fullwidth}
    after almost a generation of constant anti-American propaganda from a state-controlled radio and press, the Soviet people remain friendly to visiting Americans and are absolutely fascinated by the material blessings of American life... The most hopeful aspect of Soviet life [is that] the people are yearning for larger benefits from their hard work, that the Government is gradually showing some sympathy for this yearning and that the more this becomes true -- the less the Soviet Union can be expected to be adventuresome beyond its frontiers.
  %\end{fullwidth}
\end{quote}


\newthought{There is little doubt} that the Soviet Union lost this battle of the Cold War -- the battle to satisfy the desires of its citizens to have access to the conveniences of modern life of the sort enjoyed by their counterparts in the U.S. and other capitalist democracies.
What embittered the citizens even more was the fact that Communism derived its main legitimacy from the protection from exploitation that it afforded to the working class and yet it was not able to provide to everyone all the benefits of their collective efforts.
Comparisons with the average standard of life in the West only increased this feeling of being shortchanged
\footnote[][-5\baselineskip]{In a letter to Prisma, M. Fischer, an East-German citizen wrote, ``...twenty years after the war we must finally come to the point where our German neighboring country arrived already two years after the war, namely to be in a position to deliver perfect quality!'' From \cite{Stitziel}}
and led to increasing conviction that there was something broken with the system.
The U.S. government's propaganda attempts were also fruitful in this regard. Even though the Communist governments frowned upon Western ``intrusions'' such as jazz, western clothing, cars, cigarettes, soda etc, it did not dissuade the people from wanting access to these things, or taking drastic steps to do so.
A successful projection of the strong linkage between American style democracy and material progress also weakened grassroots support for communism in its Soviet Union.
\footnote[][-5\baselineskip]{The average age of CPSU party members started increasing throughout the 1960s and beyond as less and less young members joined the party, while more and more youth grew enamored with the West and disillusioned with their own governments.} 

The government's own complacency in believing that providing for social security and necessities of life for its citizens was enough to satisfy them only led to an undermining of the development of thriving domestic consumer culture in Eastern Europe and the Soviet Union, and helped in accelerating the end of the Cold War, bringing it to a peaceful end in 1989.

\begin{center}
Microwaves and dishwashers did defeat the bomb.
\end{center}

\backmatter

\bibliography{bibliography}
\bibliographystyle{plainnat}




\end{document}